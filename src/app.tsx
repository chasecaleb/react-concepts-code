import * as React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ExampleInfo, examplesConfig } from "./examples-config";
import { Home } from "./home";

const ExampleContainer: React.SFC<ExampleInfo> = props => {
    const ExampleComp = props.component;
    return (
        <div>
            <h1>Example: {props.path}</h1>
            <ExampleComp />
        </div>
    );
};

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={Home} exact={true} />
                    {this.exampleRoutes()}
                </Switch>
            </BrowserRouter>
        );
    }

    private readonly exampleRoutes = () =>
        examplesConfig
            .map(it => ({
                ...it,
                // Wrap the actual example component in a container.
                component: () => <ExampleContainer {...it} />
            }))
            .map(it => (
                <Route key={it.path} path={it.path} component={it.component} />
            ));
}

export default App;
