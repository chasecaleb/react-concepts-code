import * as React from "react";

interface State {
    readonly isLoggedIn: boolean;
}

export class ConditionalRendering extends React.Component<{}, State> {
    readonly state: State = { isLoggedIn: false };

    render(): React.ReactNode {
        if (!this.state.isLoggedIn) {
            return <button onClick={this.handleLogin}>Login</button>;
        }
        return <p>Top-secret access granted</p>;
    }

    private readonly handleLogin = () => this.setState({ isLoggedIn: true });
}
