import * as React from "react";

interface State {
    readonly count: number;
}

export class Counter extends React.Component<{}, State> {
    readonly state: State = { count: 0 };

    render(): React.ReactNode {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleClick}>Increment</button>
            </div>
        );
    }

    private readonly handleClick = () => {
        this.setState(prevState => ({ count: prevState.count + 1 }));
    };
}
