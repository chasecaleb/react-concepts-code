import * as React from "react";

interface Props {
    readonly items: string[];
    readonly render: (filtered: string[]) => React.ReactNode;
}

interface State {
    readonly input: string;
}

class SearchFilter extends React.Component<Props, State> {
    readonly state = { input: "" };

    private get filteredItems() {
        const filterBy = this.state.input.toLowerCase();
        return this.props.items
            .map(it => it.toLowerCase())
            .filter(it => it.indexOf(filterBy) !== -1);
    }

    render(): React.ReactNode {
        return (
            <div>
                <input
                    type="text"
                    placeholder="Search filter"
                    onChange={this.handleInput}
                />
                {this.props.render(this.filteredItems)}
            </div>
        );
    }

    private readonly handleInput = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => this.setState({ input: event.target.value });
}

export const RenderProps: React.SFC = () => (
    <SearchFilter
        items={["foo", "bar", "baz"]}
        render={filtered => (
            <div>
                Search matched {filtered.length} item(s):
                <ul>
                    {filtered.map(it => (
                        <li key={it}>{it}</li>
                    ))}
                </ul>
            </div>
        )}
    />
);
