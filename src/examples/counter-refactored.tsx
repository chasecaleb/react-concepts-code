import * as React from "react";

interface State {
    readonly count: number;
}

const CounterView: React.SFC<{
    readonly count: number;
    readonly onClick: () => void;
}> = props => (
    <div>
        <p>Count: {props.count}</p>
        <button onClick={props.onClick}>Increment</button>
    </div>
);

export class CounterContainer extends React.Component<{}, State> {
    readonly state: State = { count: 0 };

    render(): React.ReactNode {
        return (
            <CounterView count={this.state.count} onClick={this.handleClick} />
        );
    }

    private readonly handleClick = () => {
        this.setState(prevState => ({ count: prevState.count + 1 }));
    };
}
