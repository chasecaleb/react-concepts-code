import * as React from "react";

interface Props {
    readonly items: ReadonlyArray<string>;
}

const ItemList: React.SFC<Props> = props => {
    // Keys explanation: https://reactjs.org/docs/lists-and-keys.html#keys
    const itemElements = props.items.map((it, index) => (
        <li key={index}>{it}</li>
    ));
    return <ul>{itemElements}</ul>;
};

export const PropsAndLists: React.SFC = () => {
    const breakfast: string[] = ["Bacon", "Eggs", "Cheese", "Coffee"];
    return (
        <div>
            <h3>Breakfast items:</h3>
            <ItemList items={breakfast} />
        </div>
    );
};
