import * as React from "react";

interface Todo {
    readonly id: number;
    readonly title: string;
}

interface State {
    readonly todos: "LOADING" | ReadonlyArray<Todo>;
}

export class FetchApiOnMount extends React.Component<{}, State> {
    readonly state: State = { todos: "LOADING" };

    render(): React.ReactNode {
        if (this.state.todos === "LOADING") {
            return <div>Loading...</div>;
        }

        const todos = this.state.todos.map(it => (
            <li key={it.id}>{it.title}</li>
        ));
        return <ul>{todos}</ul>;
    }

    componentDidMount(): void {
        // NOTE: Error handling omitted for the sake of brevity.
        fetch("https://jsonplaceholder.typicode.com/todos/")
            .then(response => response.json())
            .then(json => this.setState({ todos: json }));
    }
}
