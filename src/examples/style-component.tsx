import * as React from "react";

// NOTE: Don't use inline styles like this in the real world.
// Use a library such as styled-components.
const Title: React.SFC = props => (
    <div
        style={{
            fontSize: "36px",
            textAlign: "center",
            textTransform: "uppercase"
        }}
    >
        {props.children}
    </div>
);

export const StyleComponent: React.SFC = () => (
    <Title>Styled title component</Title>
);
