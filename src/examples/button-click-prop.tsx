import * as React from "react";

interface Props {
    readonly handler: () => void;
}

const ActionButton: React.SFC<Props> = props => (
    <button onClick={props.handler}>Do the thing</button>
);

export const ButtonClickProp: React.SFC = () => {
    const handleClick = () => alert("Button clicked");
    return <ActionButton handler={handleClick} />;
};
