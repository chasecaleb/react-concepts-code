import * as React from "react";

interface Props {
    readonly name: string;
}

const Greeting: React.SFC<Props> = props => <h3>Hello {props.name}</h3>;

export const HelloProps: React.SFC = () => {
    const value = "React";
    return (
        <div>
            <Greeting name={value} />
            <Greeting name="Coding as Craft" />
        </div>
    );
};
