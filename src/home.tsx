import * as React from "react";
import { Link } from "react-router-dom";
import { ExampleInfo, examplesConfig } from "./examples-config";

const ExampleLink: React.SFC<ExampleInfo> = props => (
    <ul>
        <li>
            <Link to={props.path}>{props.path}</Link>
        </li>
    </ul>
);

export const Home: React.SFC = () => (
    <>
        <h1>React Concepts Examples</h1>
        {examplesConfig.map(it => (
            <ExampleLink key={it.path} {...it} />
        ))}
    </>
);
