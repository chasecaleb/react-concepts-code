import * as React from "react";
import { ButtonClickProp } from "./examples/button-click-prop";
import { ConditionalRendering } from "./examples/conditional-rendering";
import { Counter } from "./examples/counter";
import { CounterContainer } from "./examples/counter-refactored";
import { FetchApiOnMount } from "./examples/fetch-api-on-mount";
import { HelloProps } from "./examples/hello-props";
import { HelloWorld } from "./examples/hello-world";
import { PropsAndLists } from "./examples/props-and-lists";
import { RenderProps } from "./examples/render-props";
import { StyleComponent } from "./examples/style-component";

export interface ExampleInfo {
    readonly path: string;
    readonly component: React.ComponentType;
}

export const examplesConfig: ReadonlyArray<ExampleInfo> = [
    { path: "/hello-world", component: HelloWorld },
    { path: "/hello-props", component: HelloProps },
    { path: "/props-and-lists", component: PropsAndLists },
    { path: "/button-click-prop", component: ButtonClickProp },
    { path: "/conditional-rendering", component: ConditionalRendering },
    { path: "/style-component", component: StyleComponent },
    { path: "/counter", component: Counter },
    { path: "/counter-refactored", component: CounterContainer },
    { path: "/fetch-api-on-mount", component: FetchApiOnMount },
    { path: "/render-props", component: RenderProps }
];
