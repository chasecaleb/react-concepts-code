# React Concepts Code

> Accompanying examples for
> [the presentation](https://github.com/chasecaleb/react-concepts-presentation)

Bootstrapped with
[Create React App TypeScript](https://github.com/wmonk/create-react-app-typescript)

## Getting Started

-   Option A: try online at
    [https://codesandbox.io/s/github/chasecaleb/react-concepts-code]()
-   Option B: run locally
    -   Prerequisites: Node.JS and Yarn. NPM should work instead of Yarn, too.
    -   Do `yarn run start` or `npm run start` to start live dev server.

Need more info? Check out the
[Create React App User Guide](https://github.com/wmonk/create-react-app-typescript/blob/master/packages/react-scripts/template/README.md)
